package eu.zhincore.voidrunner.game;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.CacheHint;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import eu.zhincore.voidrunner.game.objects.PlayerShip.Control;

public class GameFXMLController {
  @FXML
  private Pane root;
  @FXML
  private Pane gameRoot;
  @FXML
  private Pane gameLayer;
  @FXML
  private Text fps;
  @FXML
  private Text speed;
  @FXML
  private SVGPath arrow;
  private Stage stage;

  private Game game;

  private DustManager dustManager;

  public void setStage(Stage stage) {
    this.stage = stage;

    stage.widthProperty().addListener((obs, oldVal, newVal) -> onResize());
    stage.heightProperty().addListener((obs, oldVal, newVal) -> onResize());
  }

  public void initialize() {
    game = new Game(gameLayer.getChildren());
    game.initialize();

    dustManager = new DustManager(game);

    gameLayer.setCacheHint(CacheHint.QUALITY); // Prevents leaving a trail when animating

    // Await for scene to be available
    gameLayer.sceneProperty().addListener((observable, oldScene, scene) -> {
      if (scene != null) {
        // Attach event handlers
        scene.setOnKeyPressed(event -> onKeyUpdate(event, true));
        scene.setOnKeyReleased(event -> onKeyUpdate(event, false));
        scene.setOnMousePressed(event -> onMouseUpdate(event, true));
        scene.setOnMouseReleased(event -> onMouseUpdate(event, false));
        scene.setOnMouseMoved(this::onMouseMove);
        scene.setOnMouseDragged(this::onMouseMove);
      }
    });

    anim.start();
  }

  private void onMouseUpdate(MouseEvent event, boolean pressed) {
    switch (event.getButton()) {
      case SECONDARY:
        game.getPlayer().setControl(Control.AIM, pressed);
        break;
      default:
        break;
    }
  }

  private void onKeyUpdate(KeyEvent event, boolean pressed) {
    switch (event.getCode()) {
      case W:
      case UP:
        game.getPlayer().setControl(Control.FORWARD, pressed);
        break;
      case S:
      case DOWN:
        game.getPlayer().setControl(Control.BACKWARD, pressed);
        break;
      case A:
        game.getPlayer().setControl(Control.LEFT, pressed);
        break;
      case D:
        game.getPlayer().setControl(Control.RIGHT, pressed);
        break;
      case LEFT:
        game.getPlayer().setControl(Control.YAW_LEFT, pressed);
        break;
      case RIGHT:
        game.getPlayer().setControl(Control.YAW_RIGHT, pressed);
        break;
      default:
        break;
    }
  }

  private void onMouseMove(MouseEvent event) {
    game.getPlayer().setMouse(event.getSceneX(), event.getSceneY());
  }

  private void onResize() {
    // Reset transform before scaling
    gameLayer.setTranslateX(0);
    gameLayer.setTranslateY(0);

    // Scale the game to window
    var scale = (stage.getWidth() + stage.getHeight()) / 2 / Config.REFERENCE_WINDOW_SIZE;
    gameLayer.setScaleX(scale);
    gameLayer.setScaleY(scale);

    dustManager.setSize(stage.getWidth() / scale, stage.getHeight() / scale);

    recenter();
  }

  private void recenter() {
    // Center the ship in window
    gameLayer.setTranslateX((stage.getWidth()) * 0.5 - game.getPlayer().getTranslateX() * gameLayer.getScaleX());
    gameLayer.setTranslateY((stage.getHeight()) * 0.5 - game.getPlayer().getTranslateY() * gameLayer.getScaleX());
  }

  private AnimationTimer anim = new AnimationTimer() {
    private long lastRun = 0;
    private double deltaAccumulator = 0;
    private float smoothDelta;

    @Override
    public void handle(long now) {
      if (lastRun == 0) {
        lastRun = now;
        return;
      }
      var delta = (now - lastRun) / 1_000_000f;
      deltaAccumulator += delta / 1000f;
      smoothDelta = smoothDelta * 0.9f + delta * 0.1f;
      lastRun = now;

      fps.setText(Math.round(1000 / smoothDelta) + " FPS");

      // Decouple physics from framerate
      while (deltaAccumulator >= Config.PHYSICS_TIME_STEP) {
        // Update the game
        game.update(Config.PHYSICS_TIME_STEP);

        deltaAccumulator -= Config.PHYSICS_TIME_STEP;
      }

      var shipBody = game.getPlayer().getBody();
      var v = shipBody.getLinearVelocity();
      arrow.setScaleY(Math.sqrt(Math.min(v.length(), 1f)) / 3);
      arrow.setRotate(Util.radToDeg((float) Util.getVecAngle(v)) + 90);
      speed.setText("Speed: " + Math.round(v.length() * 10) / 10f + " m/s\n     "
          + Math.round(Util.radToDeg(shipBody.getAngularVelocity()) * 10) / 10f + " °/s");

      game.render(delta);
      dustManager.update();

      recenter();
    }
  };

}
