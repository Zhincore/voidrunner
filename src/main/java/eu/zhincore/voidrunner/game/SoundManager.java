package eu.zhincore.voidrunner.game;

import java.util.ArrayList;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class SoundManager {
  static final Media THRUSTER = new Media(getResource("thruster.wav"));

  private static String getResource(String id) {
    return SoundManager.class.getResource(id).toString();
  }

  private ArrayList<MediaPlayer> players = new ArrayList<>();

  public MediaPlayer createPlayer(Media sound) {
    var player = new MediaPlayer(sound);
    players.add(player);

    return player;
  }

  public void destroyPlayer(MediaPlayer player) {
    players.remove(player);
    player.dispose();
  }
}
