package eu.zhincore.voidrunner.game;

import java.util.ArrayList;
import java.util.List;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;
import eu.zhincore.voidrunner.game.objects.Asteroid;
import eu.zhincore.voidrunner.game.objects.GameObject;
import eu.zhincore.voidrunner.game.objects.IGameObject;
import eu.zhincore.voidrunner.game.objects.PlayerShip;
import eu.zhincore.voidrunner.game.objects.Ship.Thruster;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

public class Game implements IGameObject {

  private PlayerShip player;

  private World physics = new World(new Vec2());
  private List<IGameObject> objectList = new ArrayList<>();
  private ObservableList<Node> renderList;

  Game(ObservableList<Node> renderList) {
    this.renderList = renderList;
  }

  public void initialize() {
    var path = new SVGPath();
    path.setContent(
        "m19.039 60.477 18.282-12.114v41.583l-18.248 1.1945zm-38.077 0-18.282-12.114v41.583l18.248 1.1945zm19.039 30.856 19.073-.1927-.0341-30.663c-4.2066-3.9675-10.037-6.75-19.039-7.2185-9.0015.4685-14.832 3.251-19.039 7.2185l-.0341 30.663zm6.6392-70.129 14.57-7.0352-.8892-59.457-14.066-6.2805zm-13.278 0-14.57-7.0352.8892-59.457 14.066-6.2805zm6.5837 76.016-19.235-.1215-18.03-7.153v-41.583c13.147-12.188 21.592-15.726 37.321-15.726s24.174 3.5378 37.321 15.726v41.583l-18.03 7.153-19.235.1215m-13.829-188.1-8.4047 27.184 22.178-9.648 22.178 9.648-8.4047-27.184-13.774.443zm36.34 186.8 27.856-11.052v16.01h-27.844zm-45.133 0-27.856-11.052v16.01h27.844zm22.567-196.8 10.681-.006 41.88 135.45v49.332l-33.27 13.2-19.291.1212-19.291-.1212-33.27-13.2v-49.332l41.88-135.45z");
    path.setFill(Color.TRANSPARENT);
    path.setStroke(Color.WHITE);
    path.setStrokeWidth(3);
    path.setStrokeLineJoin(StrokeLineJoin.BEVEL);

    var bg = new SVGPath();
    bg.setContent(
        "m52.561 83.899-2.138.97225v16.011h-27.844l-.01247-4.9588-3.2758 1.1755h-38.582l-3.2758-1.1755-.01237 4.9591h-27.844l-1e-4-16.011-2.138-.97225v-49.332l41.88-135.45h21.362l41.88 135.45z");
    bg.setFill(Color.BLACK);

    var shipG = new Group(bg, path);

    player = new PlayerShip(this, shipG, List.of(
        // Primary engines
        new Thruster(new Vec2(-37f, 106f), 0, 10, 27), new Thruster(new Vec2(37f, 106f), 0, 10, 27),
        // Reverse engines
        new Thruster(new Vec2(-47f, 25f), -160, 5, 20), new Thruster(new Vec2(47f, 25f), 160, 5, 20),
        // Front right
        new Thruster(new Vec2(20f, -75f), 100, 3.3f, 7),
        // Front left
        new Thruster(new Vec2(-20f, -75f), -100, 3.3f, 7),
        // Rear right
        new Thruster(new Vec2(55f, 75f), 80, 5, 10),
        // Rear left
        new Thruster(new Vec2(-55f, 75f), -80, 5, 10)

    ));
    player.addFixture(Util.svgPathToHitbox(
        "m50.423 99.064 2.138-15.165v-49.332l-41.88-135.45h-21.362l-41.88 135.45-1e-5 49.332 2.1381 15.165z"), 10);
    addObject(player);

    //

    var box = new Asteroid(this);
    box.setTransform(new Vec2(10, 10), 0);
    addObject(box);

    //
    player.toFront();
  }

  public World getPhysics() {
    return physics;
  }

  public PlayerShip getPlayer() {
    return player;
  }

  public void addObject(GameObject object) {
    objectList.add(object);
    renderList.add(object);
  }

  public void removeObject(GameObject object) {
    objectList.remove(object);
    renderList.remove(object);
  }

  public void destroy() {
    for (var object : objectList) {
      object.destroy();
    }
  }

  public void render(float delta) {
    for (var object : objectList) {
      object.render(delta);
    }
  }

  public void update(float delta) {
    for (var object : objectList) {
      object.update(delta);
    }
    physics.step(delta, 4, 2);
  }

}
