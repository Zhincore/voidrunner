package eu.zhincore.voidrunner.game;

public class Config {
  private Config() {
  }

  /** Screen size to aim for when scaling the viewport to window */
  public static final float REFERENCE_WINDOW_SIZE = 1024f;

  /** How many meters are in one pixel. */
  public static final float PHYSICS_SCALE = 1f / 100f;
  /** How many pixels are in one meter */
  public static final float PHYSICS_SCALE_INV = 1f / PHYSICS_SCALE;
  /** How often are physics calculated */
  public static final float PHYSICS_TIME_STEP = 1f / 60f;

  /** How many seconds do dust particles take to fade in and out */
  public static final double DUST_FADE = 1;
  /** Maximum speed a dust particle can get on a single axis */
  public static final float DUST_MAX_SPEED = 0.1f;
  /** Target count of dust particles in game. Increased with speed. */
  public static final int DUST_TARGET_COUNT = 32;
  /** Maximum number of particles in game at any given moment. */
  public static final int DUST_MAX_COUNT = 256;
}
