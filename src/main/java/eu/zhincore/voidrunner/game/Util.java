package eu.zhincore.voidrunner.game;

import java.util.ArrayList;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import javafx.geometry.Point2D;
import javafx.scene.shape.*;

public class Util {
  private static final double RAD_TO_DEG = 180.0 / Math.PI;
  private static final double DEG_TO_RAD = Math.PI / 180.0;

  private Util() {
  }

  public static double radToDeg(double rad) {
    return rad * RAD_TO_DEG;
  }

  public static double radToDeg(float rad) {
    return radToDeg((double) rad);
  }

  public static float degToRad(double rad) {
    return (float) (rad * DEG_TO_RAD);
  }

  public static Point2D vecToPoint(Vec2 vec) {
    return new Point2D(vec.x, vec.y);
  }

  public static Vec2 pointToVec(Point2D point) {
    return new Vec2((float) point.getX(), (float) point.getY());
  }

  /** Return a new vector rotated by given angle in radians */
  public static Vec2 rotateVec(Vec2 vec, double angle) {
    var cs = Math.cos(angle);
    var sn = Math.sin(angle);
    return new Vec2((float) (vec.x * cs - vec.y * sn), (float) (vec.x * sn + vec.y * cs));
  }

  /** Calculates the angle of a vector in radians */
  public static double getVecAngle(Vec2 vec) {
    return Math.atan2(vec.y, vec.x);
  }

  /**
   * Converts SVGPath description to a JBox2D shape and scales it by
   * PHYSICS_SCALE.
   */
  public static PolygonShape svgPathToHitbox(String d) {
    var svgPath = new SVGPath();
    svgPath.setContent(d);

    // Convert SVGPath to Path
    Path path = (Path) Shape.union(svgPath, new Circle());

    var hitbox = new PolygonShape();
    var points = new ArrayList<Vec2>();

    // Iterate over path elements and convert them to vertices
    for (var element : path.getElements()) {
      Vec2 point;

      if (element instanceof MoveTo) {
        point = new Vec2((float) ((MoveTo) element).getX(), (float) ((MoveTo) element).getY());
      } else if (element instanceof LineTo) {
        point = new Vec2((float) ((LineTo) element).getX(), (float) ((LineTo) element).getY());
      } else continue;

      points.add(point.mul(Config.PHYSICS_SCALE));
    }

    var count = points.size();
    var array = new Vec2[count];
    for (var i = 0; i < count; i++) {
      array[i] = points.get(i);
    }

    hitbox.set(array, count);

    return hitbox;
  }
}
