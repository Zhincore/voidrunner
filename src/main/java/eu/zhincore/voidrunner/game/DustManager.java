package eu.zhincore.voidrunner.game;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;
import org.jbox2d.common.Vec2;
import eu.zhincore.voidrunner.game.objects.DustParticle;
import javafx.scene.effect.MotionBlur;

public class DustManager {
  private Game game;
  private double width;
  private double height;
  private List<DustParticle> particles = new ArrayList<>();
  private SplittableRandom random = new SplittableRandom();
  private MotionBlur motionBlur = new MotionBlur();

  public DustManager(Game game) {
    super();
    this.game = game;
  }

  public void setSize(double width, double height) {
    this.width = width;
    this.height = height;
  }

  public void update() {
    var deadParticles = new ArrayList<DustParticle>();

    for (var particle : particles) {
      if (particle.getLifetime() <= 0) {
        game.removeObject(particle);
        deadParticles.add(particle);
        particle.destroy();
      }
    }

    particles.removeAll(deadParticles);

    var player = game.getPlayer();
    var position = player.getPosition();
    var velocity = player.getBody().getLinearVelocity().clone();
    var speed = velocity.normalize();
    var speedFactor = Math.sqrt(speed + 1);
    velocity.mulLocal((float) speedFactor);

    var lookahead = velocity.mul(3);
    var offset = position.add(lookahead);

    var maxLifetime = 10 / speedFactor;

    motionBlur.setAngle(Util.radToDeg(Util.getVecAngle(velocity)));
    motionBlur.setRadius(speed * 2);

    var count = Math.min(Config.DUST_TARGET_COUNT * speedFactor, Config.DUST_MAX_COUNT);
    while (particles.size() < count) {
      var particle = new DustParticle(game,
          // Position
          new Vec2((float) (width * 0.5 - random.nextDouble(width)), (float) (height * 0.5 - random.nextDouble(height)))
              .mul(Config.PHYSICS_SCALE).add(offset),
          // Velocity
          new Vec2((float) random.nextDouble(-Config.DUST_MAX_SPEED, Config.DUST_MAX_SPEED),
              (float) random.nextDouble(-Config.DUST_MAX_SPEED, Config.DUST_MAX_SPEED)),
          // Lifetime
          0.5 + random.nextDouble(maxLifetime));

      particle.setEffect(motionBlur);
      particles.add(particle);
      game.addObject(particle);
    }
  }
}
