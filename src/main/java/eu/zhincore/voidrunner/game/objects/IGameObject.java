package eu.zhincore.voidrunner.game.objects;

public interface IGameObject {
  public void destroy();

  public void render(float delta);

  public void update(float delta);
}
