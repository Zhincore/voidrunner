package eu.zhincore.voidrunner.game.objects;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import eu.zhincore.voidrunner.game.Config;
import eu.zhincore.voidrunner.game.Game;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class DustParticle extends GameObject<Circle> {
  private double lifetime;
  private double maxLifetime;

  public DustParticle(Game game, Vec2 position, Vec2 velocity, double lifetime) {
    super(game, new Circle(2), BodyType.KINEMATIC);
    this.maxLifetime = lifetime;
    this.lifetime = lifetime;

    node.setFill(Color.GRAY);
    setOpacity(0);

    body.setTransform(position, 0);
    body.setLinearVelocity(velocity);
    synchronize();
  }

  public double getLifetime() {
    return lifetime;
  }

  @Override
  public void update(float delta) {
    super.update(delta);
    lifetime -= delta;
  }

  @Override
  public void render(float delta) {
    super.render(delta);

    double fade = Math.min(Config.DUST_FADE, maxLifetime / 3);
    double factor = 1;

    if (lifetime <= fade) {
      factor = lifetime / fade;
    } else if (lifetime >= maxLifetime - fade) {
      factor = (maxLifetime - lifetime) / fade;
    }

    setOpacity(factor);
  }
}
