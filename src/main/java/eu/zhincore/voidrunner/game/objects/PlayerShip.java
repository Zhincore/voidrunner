package eu.zhincore.voidrunner.game.objects;

import java.util.EnumMap;
import java.util.Map;
import org.jbox2d.common.Vec2;
import eu.zhincore.voidrunner.game.Game;
import javafx.geometry.Point2D;
import javafx.scene.Node;

/** Wrapper for GameObject that makes it controllable */
public class PlayerShip extends Ship {
  public enum Control {
    FORWARD, BACKWARD, LEFT, RIGHT, YAW_RIGHT, YAW_LEFT, AIM
  }

  /** Map of controls and their pressed state */
  private Map<Control, Boolean> controls = new EnumMap<>(Control.class);

  /** Position of the mouse on the screen */
  private Point2D mouse;

  public PlayerShip(Game game, Node node, Iterable<Thruster> thrusters) {
    super(game, node, thrusters);

    // Initialize controls
    for (var control : Control.values()) {
      controls.put(control, false);
    }
  }

  /** Change the pressed state of a control */
  public void setControl(Control control, boolean pressed) {
    controls.put(control, pressed);
  }

  /** Get the set state of a control */
  public boolean getControl(Control control) {
    return controls.get(control);
  }

  public void setMouse(double x, double y) {
    this.mouse = new Point2D(x, y);
  }

  public Point2D getMouse() {
    return mouse;
  }

  @Override
  public void update(float delta) {
    // Calculate directional thrust
    var control = new Vec2();
    if (controls.get(Control.FORWARD).booleanValue()) {
      control.addLocal(1, 0);
    }
    if (controls.get(Control.BACKWARD).booleanValue()) {
      control.addLocal(-1, 0);
    }
    if (controls.get(Control.LEFT).booleanValue()) {
      control.addLocal(0, 1);
    }
    if (controls.get(Control.RIGHT).booleanValue()) {
      control.addLocal(0, -1);
    }
    setThrust(control);

    // Calculate angular thrust
    var angle = 0;
    if (controls.get(Control.YAW_LEFT).booleanValue()) {
      angle -= 90;
    }
    if (controls.get(Control.YAW_RIGHT).booleanValue()) {
      angle += 90;
    }
    if (angle != 0) addAngularThrust(angle);

    // Calculate aim assist
    if (mouse != null && controls.get(Control.AIM).booleanValue()) {
      var shipBounds = localToScene(getBoundsInLocal());
      var mouseAngle = mouse.subtract(shipBounds.getCenterX(), shipBounds.getCenterY()).angle(Math.cos(getAngle()),
          Math.sin(getAngle())) - 90;

      if (!Double.isNaN(mouseAngle)) {
        var stabilization = -body.getAngularVelocity() * body.getMass();

        addAngularThrust((float) (-mouseAngle * 0.1 + stabilization * 0.5));
      }
    }

    super.update(delta);
  }
}
