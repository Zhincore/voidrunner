package eu.zhincore.voidrunner.game.objects;

import java.util.ArrayList;
import java.util.Random;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;
import eu.zhincore.voidrunner.game.Config;
import eu.zhincore.voidrunner.game.Game;
import eu.zhincore.voidrunner.game.Util;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

public class Asteroid extends GameObject<Path> {

  public Asteroid(Game game) {
    super(game, new Path(), BodyType.STATIC);

    var points = new ArrayList<Vec2>();

    Random random = new Random(1);

    for (int i = 0; i < 8; i++) {
      var a = random.nextDouble();
      var x = random.nextDouble() * 50;
      var y = 100 - random.nextDouble() * 50;

      Vec2 pos = Util.rotateVec(new Vec2((float) x, (float) y), i / 8. * Math.PI * 2. + a);
      points.add(pos);

      if (i == 0) {
        node.getElements().add(new MoveTo(pos.x, pos.y));
      } else {
        node.getElements().add(new LineTo(pos.x, pos.y));
      }
    }

    node.getElements().add(new ClosePath());
    node.setStroke(Color.GRAY);
    node.setStrokeWidth(4);
    node.setStrokeType(StrokeType.INSIDE);

    var hitbox = new PolygonShape();

    var count = points.size();
    var array = new Vec2[count];
    for (var i = 0; i < count; i++) {
      array[i] = points.get(i).mul(Config.PHYSICS_SCALE);
    }

    hitbox.set(array, count);

    addFixture(hitbox, 0);

    recalculateBounds();
  }
}
