package eu.zhincore.voidrunner.game.objects;

import java.util.ArrayList;
import org.jbox2d.common.Vec2;
import eu.zhincore.voidrunner.game.Config;
import eu.zhincore.voidrunner.game.Game;
import eu.zhincore.voidrunner.game.Util;
import javafx.scene.Node;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import java.util.SplittableRandom;
import javafx.scene.shape.*;

/** GameObject with thrusters */
public class Ship extends GameObject<Node> {
  private ArrayList<Thruster> thrusters = new ArrayList<>();

  public Ship(Game game, Node node, Iterable<Thruster> thrusters) {
    super(game, node);

    for (var thruster : thrusters) {
      thruster.setShip(this);
      this.thrusters.add(thruster);
      this.getChildren().add(0, thruster);
    }
  }

  @Override
  public void destroy() {
    for (var thruster : thrusters) {
      thruster.destroy();
    }
    super.destroy();
  }

  /**
   * Makes the ship move in the given world vector, applies power to the most
   * viably placed thrusters for given direction
   */
  public void setThrust(Vec2 vector) {
    // Find most viable thrusters for given vector
    for (var thruster : thrusters) {
      if (vector.length() == 0) thruster.setPower(0);

      var product = Vec2.dot(thruster.getThrustVector(), vector);
      thruster.setPower(product < 0 ? (float) Math.pow(-product, 2) : 0);
    }
  }

  /** Makes the ship spin in the direction given by the sign of given power */
  public void addAngularThrust(float power) {
    // Find most viable thrusters for given angle direction
    if (power == 0) return;

    for (var thruster : thrusters) {
      var y = Math.signum(thruster.getPosition().y);
      var angleDt = Math.abs(90 * Math.signum(y * power) - thruster.getAngle());
      if (angleDt <= 60) {
        thruster.addPower(Math.abs(power) * (1 - angleDt / 60));
      }
    }
  }

  @Override
  public void render(float delta) {
    for (var thruster : thrusters) {
      thruster.render(delta);
    }

    super.render(delta);
  }

  @Override
  public void update(float delta) {
    for (var thruster : thrusters) {
      thruster.update(delta);
    }

    super.update(delta);
  }

  public static class Thruster extends Pane implements IGameObject {
    // Given parameters
    private Ship ship;
    private Vec2 position;
    private float angle;
    private float strength;
    private double girth;

    // Generated attributes
    private LineTo flame;
    private Vec2 vector;
    private SplittableRandom random = new SplittableRandom();

    // Dynamic attributes
    private float targetPower = 0;
    private float currentPower = 0;

    public Thruster(Vec2 position, float angle, float strength, double girth) {
      super();
      this.position = position;
      this.angle = angle;
      this.strength = strength;
      this.girth = girth;

      this.vector = Util.rotateVec(new Vec2(-1, 0), Util.degToRad(angle));

      var halfGirth = girth * 0.5;

      // Draw the flame
      var path = new Path();
      path.getElements().add(new MoveTo(-halfGirth, 0));
      path.getElements().add(new ArcTo(halfGirth, halfGirth, 0, halfGirth, 0, false, true));
      flame = new LineTo(0, 0);
      path.getElements().add(flame);
      path.getElements().add(new ClosePath());

      path.setFill(new Color(0, 1, 1, 0.8));
      path.setStroke(new Color(0, 0.2, 1, 0.5));
      path.setStrokeWidth(halfGirth * 0.75);

      this.getChildren().add(path);
      this.setEffect(new GaussianBlur(girth / 4));
      this.setMaxWidth(0);
      this.setMaxHeight(0);

      this.setRotate(-angle);
      this.setTranslateX(position.x);
      this.setTranslateY(position.y);
    }

    public void destroy() {
      //
    }

    public void setShip(Ship ship) {
      this.ship = ship;
    }

    public void setPower(float power) {
      targetPower = power;
    }

    public void addPower(float power) {
      targetPower += power;
    }

    public Vec2 getPosition() {
      return position;
    }

    public float getAngle() {
      return angle;
    }

    public Vec2 getThrustVector() {
      return vector;
    }

    public void render(float delta) {
      // Update flame
      var visualPower = Math.pow(currentPower, 0.25);
      flame.setY(visualPower * girth
          + (visualPower > 0 ? random.nextDouble(visualPower * girth * 0.1) + (delta % 16) * 0.5 * visualPower : 0));
      flame.setX(visualPower > 0 ? (random.nextDouble() - 0.5) * visualPower * girth * 0.1 : 0);
      this.setOpacity(visualPower);
    }

    public void update(float delta) {
      currentPower += (targetPower - currentPower) * delta * 10;
      if (currentPower > 1) currentPower = 1; // TODO: Boost
      if (currentPower <= 0) {
        currentPower = 0;
        return;
      }

      // Apply power
      var thrust = vector.mul(currentPower * strength);
      thrust = new Vec2(thrust.y, thrust.x);
      this.ship.getBody().applyForce(Util.rotateVec(thrust, this.ship.getAngle()),
          Util.rotateVec(position.mul(Config.PHYSICS_SCALE), this.ship.getAngle()).add(this.ship.getPosition()));
    }
  }
}
