package eu.zhincore.voidrunner.game.objects;

import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.Fixture;
import eu.zhincore.voidrunner.game.Config;
import eu.zhincore.voidrunner.game.Game;
import eu.zhincore.voidrunner.game.Util;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;

public class GameObject<T extends Node> extends StackPane implements IGameObject {
  // Given params
  protected Game game;
  protected Body body;
  protected T node;

  public GameObject(Game game, T node) {
    this(game, node, BodyType.DYNAMIC);
  }

  public GameObject(Game game, T node, BodyType type) {
    super(node);
    this.game = game;
    this.node = node;

    // Create a physics body
    var def = new BodyDef();
    def.type = type;
    body = game.getPhysics().createBody(def);

    recalculateBounds();
  }

  public void recalculateBounds() {
    var bounds = node.getBoundsInLocal();
    this.setWidth(bounds.getWidth());
    this.setHeight(bounds.getHeight());
    this.setLayoutX(-bounds.getWidth() / 2.);
    this.setLayoutY(-bounds.getHeight() / 2.);
  }

  public Fixture addFixture(Shape hitbox, float density) {
    return body.createFixture(hitbox, density);
  }

  public void removeFixture(Fixture fixture) {
    body.destroyFixture(fixture);
  }

  public void destroy() {
    game.getPhysics().destroyBody(body);
  }

  public Node getNode() {
    return node;
  }

  public Body getBody() {
    return body;
  }

  public Vec2 getPosition() {
    return body.getPosition();
  }

  /** Returns object's rotation angle in radians */
  public float getAngle() {
    return body.getAngle();
  }

  public Point2D getDirection() {
    var rads = (this.getRotate() - 90) * (Math.PI / 180);
    return new Point2D(Math.cos(rads), Math.sin(rads));
  }

  public void setTransform(Vec2 position, float angle) {
    body.setTransform(position.mul(Config.PHYSICS_SCALE), angle);
  }

  /** Synchronize visual representation with the physical one */
  public void synchronize() {
    this.setRotate(Util.radToDeg(getAngle()));

    var pos = this.body.getPosition();
    this.setTranslateX(pos.x * Config.PHYSICS_SCALE_INV);
    this.setTranslateY(pos.y * Config.PHYSICS_SCALE_INV);
  }

  public void render(float delta) {
    synchronize();
  }

  public void update(float delta) {
    // Not needed
  }
}
