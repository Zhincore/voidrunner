package eu.zhincore.voidrunner;

import eu.zhincore.voidrunner.game.GameFXMLController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    FXMLLoader loader = new FXMLLoader(getClass().getResource("App.fxml"));
    Pane root = loader.load();

    GameFXMLController controller = loader.getController();
    controller.setStage(primaryStage);

    primaryStage.setTitle("Void Runner");
    primaryStage.setScene(new Scene(root));
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
